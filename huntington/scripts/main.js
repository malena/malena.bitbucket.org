$(document).ready(function(){
	$('.single-item').slick({
		autoplay: true,
		autoplaySpeed: 6000,
		arrows: true,
		prevArrow: '<a class="prev"></a>',
		nextArrow: '<a class="next"></a>',
	});

	$('#tabs').find('.tab-title').click(function(e){
		$(this).next().slideToggle('fast');

		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
		} else {
			$('.tab-title').removeClass('open');
			$(this).addClass('open');
		}
		$(".tab-content").not($(this).next()).slideUp('fast');
	});

	$('.modal-popup').find('.modal-title').click(function(e){
		e.preventDefault();
		var content = $(this).next();
		content.fadeIn();
		var that = this;
		var modal_bg = $('.modal-bg');
		modal_bg.fadeIn();

		$('.close').click(function(){
			$(that).next().fadeOut();
			modal_bg.fadeOut();
		});
	});

	$('.contact-secondary').click(function(){
		$('.contact-primary').click();
	});
});

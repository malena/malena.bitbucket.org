var app = angular.module('AdChemix', ['ngRoute', 'ui.router', 'ui.bootstrap']);

app.config(function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('login',
        {
            url:'/login',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/login/login.html'
                }
            }
        })
        .state('dashboard',
        {
            url:'/dashboard',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/dashboard/dashboard.html'
                }
            }
        })
        .state('products', {
            url:'/products',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/products/products.html'
                }
            }
        })
        .state('product-details', {
            url:'/product-details',
             views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/products/productsDetails.html'
                }
            }
        })
        .state('categories',
        {
            url:'/categories',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/productCategories/categories.html'
                }
            }
        })
        .state('category-details',
        {
            url:'/category-details',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/productCategories/categoriesDetails.html'
                }
            }
        })
        .state('campaigns',
        {
            url:'/campaigns',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/campaigns/campaigns.html'
                }
            }
        })
        .state('campaign-details',
        {
            url:'/campaign-details',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/campaigns/campaignsDetails.html'
                }
            }
        })
        .state('adgroup',
        {
            url:'/adgroup',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/adGroup/adgroup.html'
                }
            }
        })
        .state('adgroup-details',
        {
            url:'/adgroup-details',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/adGroup/adgroupDetails.html'
                }
            }
        })
        .state('keywords',
        {
            url:'/keywords',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/keyword/keywords.html'
                }
            }
        })
        .state('keyword-details',
        {
            url:'/keyword-details',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/keyword/keywordsDetails.html'
                }
            }
        })
        .state('adcreative',
        {
            url:'/adcreative',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/adCreative/adcreative.html'
                }
            }
        })
        .state('adcreative-details',
        {
            url:'/adcreative-details',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/adCreative/adcreativeDetails.html'
                }
            }
        })
        .state('channel-details',
        {
            url:'/channel-details',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/channel/channelDetails.html'
                }
            }
        })
        .state('channel',
        {
            url:'/channel',
            views: {
                "header": {
                    templateUrl: 'app/views/modules/header.html'
                },
                "body": {
                    templateUrl: 'app/views/channel/channel.html'
                }
            }
        }
    )
});

app.run(function($state, $rootScope){
    $rootScope.$state = $state;
});
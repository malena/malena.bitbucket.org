app.directive('graphy', [function ($parse){
    function link(scope,element,attrs){
    	scope.chart = c3.generate({
        bindto: '#chart',
        data: {
            columns: [
                ['Ad Cost', 10, 20, 30, 40],
                ['Unit Cost', 30, 50, 60, 90],
                ['Profit', 40, 90, 70, 10],
                ['Revenue', 70, 90, 30, 20]
            ]
        },
        axis: {
            x: {
                type: 'category',
                categories: ['2011', '2012', '2013', '2014']
            }
        }
    });

    }

	var directiveDefinitionObject = {
		restrict: "E",
		replace: "False",
		template: "<div id='chart'></div>",
		link: link
	};

	return directiveDefinitionObject;
}]);
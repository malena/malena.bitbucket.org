app.controller("LoginController", ["$rootScope", "$scope", "$state", "Users", function ($rootScope, $scope, $state, Users){
    $scope.users = Users;

    $scope.login = function (){
        $state.go('dashboard');
    };

}]);

app.controller("KeywordsController", ["$scope", "DataService", function ($scope, DataService) {

    $scope.keywordResults =  DataService.getKeywordResults();

    $scope.total_ad_cost = 0;
    $scope.total_revenue = 0;
    $scope.total_profit = 0 ;
    $scope.total_unit_cost = 0;

    angular.forEach($scope.keywordResults, function(value, key){
        $scope.total_ad_cost = value.ad_cost + $scope.total_ad_cost;
        $scope.total_unit_cost = value.unit_cost + $scope.total_unit_cost;
        $scope.total_profit = value.profit + $scope.total_profit;
        $scope.total_revenue = value.revenue + $scope.total_revenue;
    });

    $scope.chart = c3.generate({
        bindto: '#chart',
        data: {
            columns: [
                ['Ad Cost', $scope.total_ad_cost - 30000, $scope.total_ad_cost- 20000, $scope.total_ad_cost - 10000, $scope.total_ad_cost],
                ['Unit Cost', $scope.total_unit_cost - 3000, $scope.total_unit_cost - 2000, $scope.total_unit_cost - 1000, $scope.total_unit_cost],
                ['Profit', $scope.total_profit - 6000, $scope.total_profit - 5000, $scope.total_profit - 3000, $scope.total_profit],
                ['Revenue', $scope.total_revenue - 3000, $scope.total_revenue - 40000, $scope.total_revenue - 2000, $scope.total_revenue]
            ]
        },
        axis: {
            x: {
                type: 'category',
                categories: ['2011', '2012', '2013', '2014']
            }
        }
    });

}]);

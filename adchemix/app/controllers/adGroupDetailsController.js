app.controller("AdGroupDetailsController", ["$scope", "DataService", function ($scope, DataService) {

    $scope.oneAtATime = true;

    $scope.groups = DataService.getAdGroupDetails();
    console.log($scope.groups);

    $scope.details = $scope.groups.details;
    $scope.groups = $scope.groups.sub_reports;

    $scope.header = [];

    angular.forEach($scope.groups, function(value, key){
        console.log(key);
        $scope.header.push(key);
    });

    $scope.addItem = function() {
        var newItemNo = $scope.items.length + 1;
        $scope.header.push('Item ' + newItemNo);
    };

    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };

}]);

app.controller("CampaignsDetailsController", ["$scope", "DataService", function ($scope, DataService) {

    $scope.oneAtATime = true;

    $scope.groups = DataService.getCampaignDetails();
    console.log($scope.groups);

    $scope.details = $scope.groups.details;
    console.log($scope.details);

    $scope.groups = $scope.groups.sub_reports;


    $scope.header = [];

    angular.forEach($scope.groups, function(value, key){
        $scope.header.push(key);
    });

    $scope.addItem = function() {
        var newItemNo = $scope.items.length + 1;
        $scope.header.push('Item ' + newItemNo);
    };

    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };
}]);
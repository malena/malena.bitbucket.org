app.controller("ProductsDetailsController", ["$scope", "DataService", function ($scope, DataService) {

	$scope.oneAtATime = true;

    $scope.groups = DataService.getProductDetails();
    
    $scope.details = $scope.groups.details;

    $scope.groups = $scope.groups.sub_reports;

    console.log($scope.groups);

    $scope.header = [];

    angular.forEach($scope.groups, function(value, key){
        $scope.header.push(key);
    });

    $scope.addItem = function() {
        var newItemNo = $scope.items.length + 1;
        $scope.header.push('Item ' + newItemNo);
    };

    $scope.status = {
        isFirstOpen: true,
        isFirstDisabled: false
    };
}]);
module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

		watch: {
			options:{
				livereload: true,
			},
			css: {
				files: ['app/css/*.styl'],
				tasks: ['stylus'],
				options: {
					livereload: true,
					spawn: false
				}
			},
			html: {
			    files: [
                    'app/index.jade', 
                    'app/views/*/*.jade',

                ],
				tasks: ['jade'],
				options: {
					livereload: true,
					spawn: false
				}
			}
		},

		uglify : {
			build: {
				src: ['app/services/*.js', 'app/controllers/*.js', 'app/services/*.js', 'app/directives/*.js'],
				dest: 'public/dist/js/project.min.js' 
			}
		},

		stylus: {
			options: {
				compress: true
			},
			compile: {
				files: {
					'public/dist/css/project.min.css' : 'app/css/index.styl'
				}
			}
		},

	    jade: {
            compile: {
                options: {
                    data: {
                        debug: false
                    }
                },
                files: {
                    'index.html' : 'app/index.jade',
                    'app/views/dashboard/dashboard.html' : 'app/views/dashboard/dashboard.jade',
                    'app/views/products/products.html' : 'app/views/products/products.jade',
                    'app/views/products/productsDetails.html' : 'app/views/products/productsDetails.jade',
                    'app/views/productCategories/categories.html' : 'app/views/productCategories/categories.jade',
                    'app/views/productCategories/categoriesDetails.html' : 'app/views/productCategories/categoriesDetails.jade',
                    'app/views/campaigns/campaigns.html' : 'app/views/campaigns/campaigns.jade',
                    'app/views/campaigns/campaignsDetails.html' : 'app/views/campaigns/campaignsDetails.jade',
                    'app/views/adGroup/adgroup.html' : 'app/views/adGroup/adgroup.jade',
                    'app/views/adGroup/adgroupDetails.html' : 'app/views/adGroup/adgroupDetails.jade',
                    'app/views/keyword/keywords.html' : 'app/views/keyword/keywords.jade',
                    'app/views/keyword/keywordsDetails.html' : 'app/views/keyword/keywordsDetails.jade',
                    'app/views/adCreative/adcreative.html' : 'app/views/adCreative/adcreative.jade',
                    'app/views/adCreative/adcreativeDetails.html' : 'app/views/adCreative/adcreativeDetails.jade',
                    'app/views/channel/channel.html' : 'app/views/channel/channel.jade',
                    'app/views/channel/channelDetails.html' : 'app/views/channel/channelDetails.jade',
                    'app/views/login/login.html' : 'app/views/login/login.jade',
                    'app/views/modules/header.html' : 'app/views/modules/header.jade'
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-jade');

    grunt.registerTask('default', ['stylus', 'jade', 'watch']);
};

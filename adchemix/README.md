adchemix-demoware
===========================

## Installation

Install Node.js - http://nodejs.org 

Install project's node packages

````bash
npm install
````

Run Project

````bash
grunt serve
````

You should now be able to view project at [http://localhost:5500](http://localhost:5500)

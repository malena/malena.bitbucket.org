module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

		connect: {
            server: {
                options: {
                    hostname: '*',
                    port: 8800,
                   	keepalive: true 
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default', ['connect']);

};